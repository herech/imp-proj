/**
 * Popis		               : Font ASCII znaků pro maticový LCD displej o rozměrech 5x7 bodů
 * Autor		               : Jan Herec, xherec00
 * Změny provedené v souboru   : Soubor vychází z obsahu souboru FitkitSVN\apps\demo_msp\led\mcu\main.c pro řízení LED pomocí časovače.
 *                               Změny oproti původnímu souboru: - upravena funkce decode_user_cmd()
 *                                                               - upravena funkce main()
 *                                                               - přidány globální proměnné a makra
 *                                                               - přidána funkce get_bit_val (převzata z webového zdroje, jehož URL uvedeno u této funkce)  
 *                                                               - přidána funkce set_bit (převzata z webového zdroje, jehož URL uvedeno u této funkce) 
 *                                                               - přidána funkce clear_bit (převzata z webového zdroje, jehož URL uvedeno u této funkce) 
 *                                                               - upravena funkce Timer_A()
 *                               Celkový podíl změn v souboru vůči původnímu obsahu (měřeno v poměru přidaných řádků) se pohybuje kolem 70%. 
 * Datum poslední změny		   : 14/12/2015
 * Kódování		               : UTF-8 (bez BOM)
 */

/*******************************************************************************
   main: interrupt service routine (ISR) of timer A0 utilized for LED control
   Copyright (C) 2009 Brno University of Technology,
                      Faculty of Information Technology
   Author(s): Josef Strnadel <strnadel AT stud.fit.vutbr.cz>

   LICENSE TERMS

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:
   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in
      the documentation and/or other materials provided with the
      distribution.
   3. All advertising materials mentioning features or use of this software
      or firmware must display the following acknowledgement:

        This product includes software developed by the University of
        Technology, Faculty of Information Technology, Brno and its
        contributors.

   4. Neither the name of the Company nor the names of its contributors
      may be used to endorse or promote products derived from this
      software without specific prior written permission.

   This software or firmware is provided ``as is'', and any express or implied
   warranties, including, but not limited to, the implied warranties of
   merchantability and fitness for a particular purpose are disclaimed.
   In no event shall the company or contributors be liable for any
   direct, indirect, incidental, special, exemplary, or consequential
   damages (including, but not limited to, procurement of substitute
   goods or services; loss of use, data, or profits; or business
   interruption) however caused and on any theory of liability, whether
   in contract, strict liability, or tort (including negligence or
   otherwise) arising in any way out of the use of this software, even
   if advised of the possibility of such damage.

   $Id$


*******************************************************************************/

#include <fitkitlib.h>
#include <stdlib.h>
#include <string.h>
#include "font5x7.h"

// inicializace výstupu P4OUT konstantou 0xE0 = 11100000 (nastavíme bity/piny 0 - 4 do nuly - sloupce nesvítí)
#define INIT_P4OUT (P4OUT & 0xE0)

// pokud chceme pomocí orování (|) nastavit některé bity P4OUT do '1', tak unsigned char, kterým chceme P4OUT orovat, musíme předtím andovat (&) s touto maskou MASK_P4OUT - abychom nezapsali '1' do bitů, které jsou víceúčelové a používané jinými komponentami FITkitu
#define MASK_P4OUT (0x1F) // 00011111

// inicializace výstupu P2OUT konstantou 0x07 = 00000111 (nastavíme bity/piny 3 - 7 do nuly - sloupce nesvítí)
#define INIT_P2OUT (P2OUT & 0x07)

// pokud chceme pomocí orování (|) nastavit některé bity P2OUT do '1', tak unsigned char, kterým chceme P2OUT orovat, musíme předtím andovat (&) s touto maskou MASK_P2OUT - abychom nezapsali '1' do bitů, které jsou víceúčelové a používané jinými komponentami FITkitu
#define MASK_P2OUT (0xF8) // 11111000

// inicializace výstupu P6 konstantou 0x7F = 01111111 (nastavíme bity/piny 0 - 7 do jedničky - řádky nesvítí)
#define INIT_P6OUT (P6OUT | 0x7F)

// posuneme se na další řádek (vytvoříme vektor bitů, který jej aktivuje)
#define NEXT_ROW ((P6OUT << 1) | 1)

#define TRUE 1
#define FALSE 0

// maximální délka vypisované zprávy (v praxi kvůli omezení terminálu je mnohem nižší)
#define ARRAY_LENGTH 100

// index aktivního řádku, který se má vysvítit
unsigned char current_active_row;

int interruptCount = 0; // čítač počtu přerušení

// příznak, jestli má obsluha přerušení čítače Timer_A počítat dané přerušení do interruptCount
int dont_count = FALSE;

// index znaku ve vstupním textu z terminálu, jehož část se vykresluje na pravém displeji 
unsigned char znak = 0;

// pole unsigned charů (vektorů), představující aktivní sloupce pro každý řádek obou displejů 
// pole jsou dočasné, probíhá nad nimi výpočet pro posunutí znaku, který se následně duplikuje do RowsVectorForDisplay11 a RowsVectorForDisplay22
// aktivní pouze při duplikování hodnot, jinak neaktivní
unsigned char RowsVectorForDisplay1[7] = {0, 0, 0, 0, 0, 0, 0, 0}; // Levý displej
unsigned char RowsVectorForDisplay2[7] = {0, 0, 0, 0, 0, 0, 0, 0}; // Pravý displej

// pole unsigned charů (vektorů), představující aktivní sloupce pro každý řádek
// aktivní v každém čase a při výpočtu posunutí znaku, neaktivní při duplikování hodnot z RowsVectorForDisplay1 a RowsVectorForDisplay2
unsigned char RowsVectorForDisplay11[7] = {0, 0, 0, 0, 0, 0, 0, 0}; // Levý displej
unsigned char RowsVectorForDisplay22[7] = {0, 0, 0, 0, 0, 0, 0, 0}; // Pravý displej

char retezec[ARRAY_LENGTH]; // vypisovaná zpráva na displeje

// příznak pro obsluhu přerušení Timer_A, který určuje, jestli probíhá duplikace hodnot z vektorů RowsVectorForDisplayx do RowsVectorForDisplayxx
// a tedy určuje které vektory/pole má pro vysvicování sloupců v daném řádku používat RowsVectorForDisplayx nebo RowsVectorForDisplayxx
unsigned char dont_change_colums = FALSE; 


/*******************************************************************************
 * Vypis uzivatelske napovedy (funkce se vola pri vykonavani prikazu "help")
 * systemoveho helpu
*******************************************************************************/
void print_user_help(void)
{
}


/*******************************************************************************
 * Dekodovani a vykonani uzivatelskych prikazu
*******************************************************************************/
unsigned char decode_user_cmd(char *cmd_ucase, char *cmd)
{
    // inicializujeme řetězec vypisované zprávy
    memset( retezec, '\0', sizeof(char)*ARRAY_LENGTH );
    
    // uložíme do jako vypisovanou zprávu řetězec z terminálu
    int i;
    for (i = 0; i < ARRAY_LENGTH - 5; i++) {
        if (cmd[i] == '\0') { break;}
        retezec[i] = cmd[i];
    }
    // přidáme na konec vypisovaného řetězce 5 mezer - slouží pro oddělení opakovaného výpisu řetězce
    retezec[i] = ' '; retezec[i + 1] = ' '; retezec[i + 2] = ' '; retezec[i + 3] = ' '; retezec[i + 4] = ' ';
    
    // nastavime index aktualniho znaku na 0 - pokud se vypisoval text, tak se zacne vypisovat novy text
    znak = 0;
    
    term_send_str("Poslano na displej: ");
    term_send_str((unsigned char *)retezec);
    return USER_COMMAND;
}


/*******************************************************************************
 * Inicializace periferii/komponent po naprogramovani FPGA
*******************************************************************************/
void fpga_initialized()
{
  term_send_crlf();
  term_send_str_crlf("Aplikace bezi. Muzete zadat retezec, ktery se vypise jako novinka na LED displejich. Zadanim mezery zrusite vypis na LED displejich.");
}

// Funkce vrátí hodnotu bitu na dané pozici v bajtu
// Převzato ze stránek http://stackoverflow.com/questions/7211069/access-to-nth-bit-without-a-conditional-statement
int get_bit_val (unsigned char val, int bitnum)
{
    if ((val & (1 << bitnum)) != 0) {
        return 1;
    }
    else {
        return 0;
    }
}

// Funkce nastaví hodnotu bitu na '1' na dané pozici v bajtu
// Převzato ze stránek http://stackoverflow.com/questions/47981/how-do-you-set-clear-and-toggle-a-single-bit-in-c-c
void set_bit(unsigned char *val, int bitnum) {
    *val |= 1 << bitnum;
}

// Funkce nastaví hodnotu bitu na '0' na dané pozici v bajtu
// Převzato ze stránek http://stackoverflow.com/questions/47981/how-do-you-set-clear-and-toggle-a-single-bit-in-c-c
void clear_bit(unsigned char *val, int bitnum) {
    *val &= ~(1 << bitnum);
}

/*******************************************************************************
 * Hlavni funkce
*******************************************************************************/
void main(void)
{
  initialize_hardware();
  WDG_stop();                               // zastav watchdog
  
  // inicializujeme řetězec vypisované zprá
  memset( retezec, '\0', sizeof(char)*ARRAY_LENGTH );
  
  // nastavíme index aktuálně vysvíceného řádku na 0
  current_active_row = 0;
  
  // inicializujeme počet přerušení, která byla vyvolána časovačem
  interruptCount = 0;
  
  // nastavíme použité porty na výstupní
  P6DIR |= 0x7F; // 01111111 
  P4DIR |= 0x1F; // 00011111 
  P2DIR |= 0xF8; // 11111000 
  
  // Pro aktuální řádek zůstanou na levém displeji všechny ledky zhasnuté
  P4OUT = INIT_P4OUT;
  
  // Pro aktuální řádek zůstanou na pravém displeji všechny ledky zhasnuté
  P2OUT = INIT_P2OUT;
  
  // Výchozím aktivním řádkem, bude řádek 1
  P6OUT = INIT_P6OUT & 0xFE;


  CCTL0 = CCIE;            // povol preruseni pro casovac (rezim vystupni komparace) 
  CCR0 = 0x005D;           // nastav po kolika ticich ma dojit k preruseni - 32768 / (50 * 7 ) = 93 (0x5D), tzn. po 93 ticich dojde k přerušení, tj. (50 * 7) za sekundu | 50hz doporučuje tento článek: http://www.best-microcontroller-projects.com/led-dot-matrix-display.html
  TACTL = TASSEL_1 + MC_2; // ACLK (f_tiku = 32768 Hz = 0x8000 Hz), nepretrzity rezim
  
  int bit_val;                 // pomocná proměnná pro uchování hodnoty bitu
  int num_of_shifts = 0;       // počet posunutí znaku po sloupích (v rozsahu 0 až 5, pak se opět nuluje)
  int delimiter_here = FALSE;  // příznak, jestli je třeba nechat volný jeden sloupec jako oddělovač mezí písmeny
  
  // nekonečná smyčka aplikace
  while (1) {
        // uplynula doba 77 ms = což odpovídá 27x spuštěné obsluze přerušení (((1 / (7 * 50) (Hz)) (s)) * 27) (s)  , je načase posunou znaky
        if (interruptCount >= 27 && (retezec[znak] != '\0')) {
          
          dont_count = TRUE;  // vypneme započítávání přerušení do interruptCount
          interruptCount = 0; // inicializujeme počet přerušení 
          int row;            // pomocná proměnná pro indexování řádku
          
          // nastavím posunutí znaku na levém displeji doleva o jeden sloupec
          for (row = 0; row < 7; row++) { 

            // shiftnu vektor vysvícených sloupců pro aktuální řádek doprava (ve skutečnosti to znamená posun znaku doleva)
            RowsVectorForDisplay1[row] = RowsVectorForDisplay1[row] >> 1;
            
            // na poslední sloupci pro aktuální řádek levého displeje nastavím bit (svítí nesvítí), který je v prvním sloupci aktuálního řádku pravého displeje
            bit_val = get_bit_val(RowsVectorForDisplay2[row], 0); 
            if (bit_val == 1) {
                set_bit(&RowsVectorForDisplay1[row], 4); 
            }
            else {
                clear_bit(&RowsVectorForDisplay1[row], 4); 
            }
          }
          
         // nastavím posunutí znaku na pravém displeji doleva o jeden sloupec
          for (row = 0; row < 7; row++) { 

             // shiftnu vektor vysvícených sloupců pro aktuální řádek doprava (ve skutečnosti to znamená posun znaku doleva)
            RowsVectorForDisplay2[row] = RowsVectorForDisplay2[row] >> 1;
            
            // dalsi bit zde nastavime, jen pokud nevykreslujeme nove pismenko a nechceme nechat aspoň jedne slupec mezeru
            if ( delimiter_here == FALSE) {
                // na poslední sloupci pro aktuální řádek pravého displeje nastavím bit (svítí nesvítí), který odpovídá příslušné aktuální pozici daného znaku
                bit_val = get_bit_val (Font5x7[(unsigned char) retezec[znak]][num_of_shifts], row);
                if (bit_val == 1) {
                    set_bit(&RowsVectorForDisplay2[row], 4); 
                }
                else {
                    clear_bit(&RowsVectorForDisplay2[row], 4); 
                }
            }
          }
          // pokud zde nevkládáme mezeru o velikosti jednoho sloupce mezi písmenky, tak provedeme inkrementování počtu posunů znaků po sloupcích
          if (delimiter_here == FALSE) {
            num_of_shifts++;
          }
          delimiter_here = FALSE;    // implicitně předpokládáme že posunujem jeden znak a nechceme dělat mezi znaky mezeru
          dont_change_colums = TRUE; // změníme vektory pro vysvicování sloupců pro řádky z RowsVectorForDisplayxx na RowsVectorForDisplayx
          
          // překopíruji vektory pro oba displeje
          for (row = 0; row < 7; row++) { 
            RowsVectorForDisplay11[row] = RowsVectorForDisplay1[row]; 
          }
          for (row = 0; row < 7; row++) { 
            RowsVectorForDisplay22[row] = RowsVectorForDisplay2[row]; 
          }
          
          // pokud jsem provedl posun znaku po sloupcích 5x (celkově tak znak prošel celým pravým displejem), 
          // tak načtu další znak a nastvím příznak oddělovače jednoho volného sloupce
          if (num_of_shifts == 5) {
            znak++; 
            num_of_shifts = 0;
            delimiter_here = TRUE;
          }
          
          dont_change_colums = FALSE; // změníme vektory pro vysvicování sloupců pro řádky z RowsVectorForDisplayx na RowsVectorForDisplayxx
          dont_count = FALSE;         // zapneme započítávání přerušení do interruptCount
        }
        // pokud je konec retezce, nastavime opet jeho zacatek
        else if (retezec[znak] == '\0') {
            znak = 0;
        }
        
      terminal_idle(); // obsluha terminalu
  }
}

/*******************************************************************************
 * Obsluha preruseni casovace timer A0
*******************************************************************************/
interrupt (TIMERA0_VECTOR) Timer_A (void)
{
  
    // na základě příznaku dont_change_colums se rozhodnu které vektory sloupců budu používat pro vysvícení
    if (dont_change_colums == FALSE) {
    // vytáhnutí vektoru z matice pro daný řádek
      P4OUT = INIT_P4OUT | (MASK_P4OUT & RowsVectorForDisplay11[current_active_row]); 
      P2OUT = INIT_P2OUT | (MASK_P2OUT & (RowsVectorForDisplay22[current_active_row] << 3)); // potřebujeme provést posunutí o 3 bity doleva přes maskováním
    }
    else {
      // vytáhnutí vektoru z matice pro daný řádek
      P4OUT = INIT_P4OUT | (MASK_P4OUT & RowsVectorForDisplay1[current_active_row]); 
      P2OUT = INIT_P2OUT | (MASK_P2OUT & (RowsVectorForDisplay2[current_active_row] << 3));  // potřebujeme provést posunutí o 3 bity doleva přes maskováním
    }
    
    // pokud se aktuálně nepočítá posun znaků, tak můžu počítat čas do dalšího posunu
    if (dont_count == FALSE) {
        interruptCount++;
    }
    
    // Nastavíme svit na dalším řádku
    P6OUT = (current_active_row != 0) ? (INIT_P6OUT) & NEXT_ROW : (INIT_P6OUT) & (NEXT_ROW & 0xFE); // 11111110 
    
    // nastavení dalšího řádku
    current_active_row = (current_active_row + 1) % 7;
    
    CCR0 += 0x005D;  // nastav po kolika ticich ma dojit k preruseni - 32768 / (50 * 7 ) = 93 (0x5D), tzn. po 93 ticich dojde k přerušení, tj. (50 * 7) za sekundu | 50hz doporučuje tento článek: http://www.best-microcontroller-projects.com/led-dot-matrix-display.html
}
